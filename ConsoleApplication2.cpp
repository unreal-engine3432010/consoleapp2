

#include <iostream>
#include <string>

int main()
{
    std::string outputString = "Hello World! This is C++ app";
    std::cout << outputString + "\n";
    std::cout << outputString.length() << "\n";
    std::cout << outputString.front() << "\n";
    std::cout << outputString.back() << "\n";
}

